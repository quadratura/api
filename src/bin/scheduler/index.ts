import axios from 'axios';
import { CronJob } from 'cron';
import * as dotenv from 'dotenv';
dotenv.config();

const TWO_DAY_AGO = 2;

import mongoose from 'mongoose';
import { MONGO_DSN, CLEANUP_CRON, CRON_HEALTHCHECK } from '../../config';
import { FlatModel } from '../../database';
import job from '../../jobs/archiveOldFlats/index';

function getDaysAgo(days: number): Date {
  const nowInSeconds = Math.round(new Date().getTime());
  const tenDaysInSeconds = 60 * 60 * 24 * days * 1000;

  return new Date(nowInSeconds - tenDaysInSeconds);
}

function startScheduler() {
  new CronJob(CLEANUP_CRON, async () => {
    axios.get(CRON_HEALTHCHECK);
    job().then(console.log);
  }).start();
}
(async function main() {
  await mongoose.connect(MONGO_DSN, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true,
    useUnifiedTopology: true,
  });

  startScheduler();
})();
