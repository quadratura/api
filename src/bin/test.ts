import * as dotenv from 'dotenv';
dotenv.config();

import job from '../jobs/archiveOldFlats/index';
import mongoose from 'mongoose';
import { MONGO_DSN } from '../config';

(async function main() {
  await mongoose.connect(MONGO_DSN, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true,
    useUnifiedTopology: true,
  });

  job()
    .then(console.log)
    .then(() => process.exit(0));
})();
