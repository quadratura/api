import * as dotenv from 'dotenv';
dotenv.config();

import mongoose from 'mongoose';
import { PORT, MONGO_DSN, SENTRY_DSN } from './config';
import { server } from './server';
import * as Sentry from '@sentry/node';

Sentry.init({ dsn: SENTRY_DSN });

mongoose.connect(MONGO_DSN, {
  useNewUrlParser: true,
  useFindAndModify: false,
  useCreateIndex: true,
  useUnifiedTopology: true,
});

server
  .listen({ port: PORT })
  .then(({ url }) => console.log(`🚀  Server ready at ${url}`));
