import { ApolloServer, gql, AuthenticationError } from 'apollo-server';
import { FlatModel } from '../database';
import { IN_PRODUCTION, ENGINE_API_KEY, ENGINE_SCHEMA_TAG } from '../config';
import { IGeoLocation, IPriceRange, ITag } from '../interfaces';
import { verify } from '../services';

export const typeDefs = gql`
  type GeoLocation {
    type: String!
    coordinates: [String]
  }
  type FlatImage {
    url: String!
  }
  type FlatTag {
    text: String
    kind: String
  }
  type FlatSource {
    url: String!
    name: String!
  }
  type Flat {
    url: String!
    geo_location: GeoLocation
    price: String
  }

  extend type Flat {
    id: String
    text: String!
    images: [FlatImage]
    size: String
    tags: [FlatTag]
  }
  extend type Flat {
    source: FlatSource
  }

  input GeoPoint {
    lng: Float!
    lat: Float!
  }
  input GeoLocationQuery {
    ne: GeoPoint!
    nw: GeoPoint!
    se: GeoPoint!
    sw: GeoPoint!
  }
  input TagQuery {
    kind: String!
    text: String!
  }
  input PriceRange {
    start: Int!
    end: Int!
  }

  type Query {
    search(
      geo_location: GeoLocationQuery
      price_range: PriceRange
      tags: [TagQuery]
      limit: Int
      skip: Int
    ): [Flat]
    all_flats(page: Int = 1, per_page: Int = 500): [Flat]
    total: Int
  }
  type Mutation {
    save(flat_as_json: String!): Flat
    deleteFlat(id: String!): Flat
  }
`;

const resolvers = {
  Query: {
    total: async () => {
      return FlatModel.estimatedDocumentCount();
    },
    all_flats: async (
      _: any,
      { page, per_page }: { page: number; per_page: number }
    ) => {
      return FlatModel.find()
        .limit(page)
        .skip((page - 1) * per_page);
    },
    search: async (
      _: any,
      {
        geo_location,
        tags,
        limit = 9_999_999_999,
        skip = 0,
        price_range = null,
      }: {
        geo_location: IGeoLocation;
        tags: ITag[];
        limit: number;
        skip: number;
        price_range: IPriceRange;
      }
    ) => {
      const query: { geo_location?: any; $and?: any; price?: any } = {};

      /**
       * Price range including border values
       */
      if (Boolean(price_range)) {
        query.price = {
          $lte: price_range.end,
          $gte: price_range.start,
        };
      }

      if (Boolean(geo_location)) {
        query.geo_location = {
          $geoWithin: {
            $geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [geo_location.nw.lng, geo_location.nw.lat],
                  [geo_location.sw.lng, geo_location.sw.lat],
                  [geo_location.se.lng, geo_location.se.lat],
                  [geo_location.ne.lng, geo_location.ne.lat],
                  [geo_location.nw.lng, geo_location.nw.lat],
                ],
              ],
            },
          },
        };
      }

      if (Boolean(tags)) {
        /**
         * MongoDB query
         * https://stackoverflow.com/questions/25014699/mongodb-multiple-elemmatch
         * {
         *  $and: [
         *    {"tags": { $elemMatch: { kind: "location_neighborhood", text: "Cvijićeva" } } },
         *    {"tags": { $elemMatch: { kind: "purchase_option", text: "sell" } } }
         *  ]
         * }
         */
        query.$and = tags.map(tag => ({
          tags: { $elemMatch: { kind: tag.kind, text: tag.text } },
        }));
      }

      /**
       * If query is empty return empty array
       * do not return all possible records
       */
      if (Object.keys(query).length === 0) {
        return [];
      }

      return FlatModel.find(query, 'url price geo_location')
        .limit(limit)
        .skip(skip);
    },
  },
  Mutation: {
    save: async (_: any, { flat_as_json }: { flat_as_json: string }) => {
      const flat: any = JSON.parse(flat_as_json);
      return FlatModel.findOneAndUpdate({ url: flat.url }, flat, {
        upsert: true,
        new: true,
      });
    },
    deleteFlat: async (_: any, { id }: { id: string }) => {
      return FlatModel.findOneAndDelete({ _id: id });
    },
  },
};

export const server = new ApolloServer({
  typeDefs,
  resolvers,
  engine: {
    apiKey: ENGINE_API_KEY,
    schemaTag: ENGINE_SCHEMA_TAG,
  },
  mocks: false,
  introspection: !IN_PRODUCTION,
  playground: !IN_PRODUCTION,
  debug: !IN_PRODUCTION,
  context: async ({ req }) => {
    const auth_header = req && req.headers && req.headers.authorization;

    if (!Boolean(auth_header)) {
      throw new AuthenticationError('Not authorized 401');
    }

    const [type, token_string] = auth_header.trim().split(' ');
    if (type !== 'Bearer') {
      throw new AuthenticationError('Not authorized 401');
    }

    try {
      const jwt = await verify(token_string);

      return { user: jwt, jwt };
    } catch (e) {
      console.error(e);
      throw new AuthenticationError('Not authorized 401');
    }
  },
});
