import mongoose from 'mongoose';
import { EnumTagTypes } from '../interfaces';

const FlatBlueprint = {
  text: String,
  url: String,
  source: {
    url: String,
    name: String,
  },
  geo_location: {
    type: {
      type: String, // Don't do `{ location: { type: String } }`
      enum: ['Point'], // 'location.type' must be 'Point'
      required: true,
    },
    // https://docs.mongodb.com/manual/geospatial-queries/#geospatial-geojson
    coordinates: {
      // If specifying latitude and longitude coordinates, list the longitude first and then latitude
      type: [Number],
      required: true,
    },
  },
  images: [{ url: String }],
  size: String,
  price: Number,
  tags: [
    {
      text: String,
      kind: {
        type: String,
        enum: Array.from(Object.values(EnumTagTypes)),
        required: true,
      },
    },
  ],
};

const FlatSchema = new mongoose.Schema(FlatBlueprint, { timestamps: true });
FlatSchema.index({ geo_location: '2dsphere' })
  .index({ url: 1 })
  .index({ price: 1 })
  .index({ 'tags.kind': 1 })
  .index({ 'tags.text': 1 });

export const FlatModel = mongoose.model('Flat', FlatSchema);

export const ArchivedFlatModel = mongoose.model(
  'ArchivedFlat',
  new mongoose.Schema(
    { flat: { ...FlatBlueprint, createdAt: Date, updatedAt: Date } },
    { timestamps: true }
  )
);
