import axios from 'axios';
const SLACK_HOOK_URL =
  'https://hooks.slack.com/services/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';
const SLACK_CHANNEL = '#background-jobs';
const SLACK_USERNAME = 'webhookbot';

export default async function(
  totalNumberOfFlatsArchived: any
): Promise<boolean> {
  await axios.post(SLACK_HOOK_URL, {
    text: `Successfully archived ${totalNumberOfFlatsArchived} flats`,
    channel: SLACK_CHANNEL,
    username: SLACK_USERNAME,
  });

  return true;
}
