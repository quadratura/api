import loadFlats from './loadFlats';
import archiveFlats from './archiveFlats';
import sendSlackUpdate from './sendSlackUpdate';

export default async function execute() {
  try {
    let result = null;

    for (let step of [loadFlats, archiveFlats, sendSlackUpdate]) {
      result = await step(result);
    }

    return true;
  } catch (e) {
    console.error(e);

    return false;
  }
}
