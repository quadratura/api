import { FlatModel } from '../../database';
import { Document } from 'mongoose';

const TWO_DAY_AGO = 2;

function getDaysAgo(days: number): Date {
  const nowInSeconds = Math.round(new Date().getTime());
  const tenDaysInSeconds = 60 * 60 * 24 * days * 1000;

  return new Date(nowInSeconds - tenDaysInSeconds);
}

export default async function(): Promise<Document[]> {
  return FlatModel.find()
    .or([
      {
        createdAt: { $lt: getDaysAgo(TWO_DAY_AGO) },
      },
      {
        updatedAt: { $lt: getDaysAgo(TWO_DAY_AGO) },
      },
    ])
    .exec();
}
