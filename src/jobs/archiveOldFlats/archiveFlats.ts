import { ArchivedFlatModel, FlatModel } from '../../database';

export default async function(flats: any[]): Promise<number> {
  const objects = flats.map(flat => ({ flat }));
  const size = 1000;
  const total = flats.length;

  for (let i = 1; i * size < total + size; i++) {
    await ArchivedFlatModel.insertMany(objects.slice((i - 1) * size, i * size));
  }
  await FlatModel.deleteMany({ id: { $in: flats.map(flat => flat._id) } });

  return flats.length;
}
