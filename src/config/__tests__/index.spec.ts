import * as config from '../index';

test('it will get config parameters', () => {
  expect(config).toHaveProperty('MONGO_DSN');
  expect(config).toHaveProperty('PORT');
  expect(config).toHaveProperty('IN_PRODUCTION');
  expect(config).toHaveProperty('AUTH0_AUDIENCE');
  expect(config).toHaveProperty('AUTH0_DOMAIN');
  expect(config).toHaveProperty('SENTRY_DSN');
  expect(config).toHaveProperty('ENGINE_API_KEY');
  expect(config).toHaveProperty('ENGINE_SCHEMA_TAG');
});
